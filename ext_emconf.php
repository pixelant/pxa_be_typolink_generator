<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'BE typolink generator',
    'description' => 'Provides viewhelper to create a FE link in BE',
    'category' => 'module',
    'author' => 'Andriy Oprysko',
    'author_email' => '',
    'author_company' => '',
    'state' => 'stable',
    'createDirs' => '',
    'clearCacheOnLoad' => false,
    'version' => '1.0.4',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-8.9.99'
        ],
        'conflicts' => [],
        'suggests' => []
    ]
];
