<?php

namespace Pixelant\PxaBeTypolinkGenerator\ViewHelpers;

use Pixelant\PxaBeTypolinkGenerator\Utility\TypoLinkUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

/**
 * Class TypoLinkViewHelper
 * @package Pixelant\PxaBeTypolinkGenerator\ViewHelpers
 */
class TypolinkViewHelper extends AbstractTagBasedViewHelper
{
    /**
     * Tag
     *
     * @var string
     */
    protected $tagName = 'a';

    /**
     * Register arguments
     */
    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerUniversalTagAttributes();
        $this->registerArgument('pageUid', 'int', 'Page UID', true);
        $this->registerArgument('additionalParameters', 'array', 'Additional link paramenters', false, []);
        $this->registerTagAttribute('target', 'string', 'Target of link', false);
        // @codingStandardsIgnoreStart
        $this->registerTagAttribute('rel', 'string', 'Specifies the relationship between the current document and the linked document', false);
        // @codingStandardsIgnoreEnd
    }

    /**
     * Generate link within BE environment
     *
     * @return string
     */
    public function render()
    {
        $pageUid = (int)$this->arguments['pageUid'];

        $uri = TypoLinkUtility::getLinkUri($pageUid, $this->arguments['additionalParameters']);

        if ((string)$uri !== '') {
            $this->tag->addAttribute('href', $uri);
            $this->tag->setContent($this->renderChildren());
            $result = $this->tag->render();
        } else {
            $result = $this->renderChildren();
        }

        return $result;
    }
}
