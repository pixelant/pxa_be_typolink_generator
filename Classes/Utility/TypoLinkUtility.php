<?php

namespace Pixelant\PxaBeTypolinkGenerator\Utility;

use TYPO3\CMS\Backend\Utility\BackendUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\StringUtility;

/**
 * Class TypoLinkUtility
 * @package Pixelant\PxaBeTypolinkGenerator\Utility
 */
class TypoLinkUtility
{
    /**
     * Cache generate links for multiple calls
     * key sha1 hash to value link
     *
     * @var array
     */
    public static $cacheLinks = [];

    /**
     * Get typolink
     *
     * @param int $pageUid
     * @param array $additionalParameters
     * @param bool $forceNoCache
     * @return string
     */
    public static function getLinkUri(
        int $pageUid,
        array $additionalParameters = [],
        bool $forceNoCache = false
    ): string {
        if (!empty($additionalParameters)) {
            $additionalParameters = GeneralUtility::implodeArrayForUrl('', $additionalParameters);
        }
        $data = [
            'id' => $pageUid,
        ];

        if (!empty($additionalParameters) && StringUtility::beginsWith($additionalParameters, '&')) {
            $data['additionalParameters'] = $additionalParameters;
        }
        // Generate hash for cache
        $hash = sha1(implode('', $data));

        // Check if in cache
        if (array_key_exists($hash, self::$cacheLinks) && !$forceNoCache) {
            return self::$cacheLinks[$hash];
        }

        $siteUrl = self::getSiteUrl($pageUid);
        if ($siteUrl) {
            $url = $siteUrl . 'index.php?eID=pxa_be_typolink_generator&data=' . base64_encode(json_encode($data));
            // Send TYPO3 cookies as this may affect path generation
            $headers = [
                'Cookie: fe_typo_user=' . $_COOKIE['fe_typo_user']
            ];

            if ($url = GeneralUtility::getURL($url, false, $headers)) {
                // Save in cache
                self::$cacheLinks[$hash] = $url;

                return $url;
            }
        }

        return '';
    }

    /**
     * Obtains site URL.
     *
     * @static
     * @param int $pageId
     * @return string
     */
    protected static function getSiteUrl(int $pageId): string
    {
        $domain = BackendUtility::firstDomainRecord(
            BackendUtility::BEgetRootLine($pageId)
        );

        $scheme = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') ? 'https' : 'http';

        return $domain ? $scheme . '://' . $domain . '/' : GeneralUtility::getIndpEnv('TYPO3_SITE_URL');
    }
}
